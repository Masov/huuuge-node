git clone https://bitbucket.org/ev45ive/huuuge-node.git
cd huuuge-node
npm i
npm run start:dev

# Updates to fork
git remote add upstream https://bitbucket.org/ev45ive/huuuge-node.git
git pull -f upstream master

# Init
npm init -y


# Update node
curl https://www.npmjs.com/install.sh | sh

# NVM
nvm ls-remote
nvm install <version> 
nvm use <version>

# Typescript
npm i -g typescript
tsc --init

npm i @types/node

# Watch
npm i nodemon
tsc --watch ./src
nodemon -w ./dist --respawn ./dist/index.js

npm i ts-node ts-node-dev

# Modules
npm i express @types/express

# 3rd party middleware
npm i express-request-id cookie-parser express-session cors errorhandler morgan connect-timeout 

# Passport

# HATEOAS
http://restify.com/docs/client-guide/


# Mocha TDD
npm install --global mocha
npm install -D mocha


# Validators
https://github.com/typestack/class-validator
https://github.com/typestack/class-transformer

https://ajv.js.org/#validation-errors 
https://joi.dev/api/?v=17.2.1

https://json-schema.org/implementations.html#editors

# ORM / ODM
https://mongoosejs.com/
https://sequelize.org/
https://typeorm.io/#/

# TypeORM
https://typeorm.io/#/

tsconfig.json:
    /* Experimental Options */
    "experimentalDecorators": true,        /* Enables experimental support for ES7 decorators. */
    "emitDecoratorMetadata": true,         /* Enables experimental support for emitting type metadata for decorators. */

npm install pg --save

# Docker postgres
"db": "docker run --name db -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres",

docker run -p 80:80 \
    -e 'PGADMIN_DEFAULT_EMAIL=user@domain.com' \
    -e 'PGADMIN_DEFAULT_PASSWORD=SuperSecret' \
    -d dpage/pgadmin4
    
docker inspect db --format "{{ .NetworkSettings.IPAddress }}"

# Orm CLI
npm run typeorm -- entity:create -n Product
npm run typeorm -- entity:create -n UserProfile

$ npm run typeorm -- migration:generate -n UserProfile_and_Products


# Notifying / Events
https://github.com/EventEmitter2/EventEmitter2


# PM2
npm install pm2 -g

# Ankieta

https://tiny.pl/7j2pc/ 


# Worker threads
https://nodesource.com/blog/worker-threads-nodejs/
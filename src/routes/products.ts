import { Router } from "express";

const allProducts = [
  { id: 1, category: 24, name: "Turbo pompka", description: "Lorem impsum" },
  { id: 2, category: 24, name: "Tenteges", description: "Lorem impsum" },
  { id: 3, category: 22, name: "Multiwstrzykator 2021", description: "Lorem impsum" },
  { id: 4, category: 12, name: "NotSoHuuuge Game", description: "Lorem impsum" },
  { id: 5, category: 6, name: "Turbo pompka premium", description: "Lorem impsum" },
];

export const productsRoutes = Router()
  .get("/", (req, res) => {
    res.send(allProducts);
  })
  .get("/:id", (req, res) => {
    res.send(allProducts.filter((_) => _.id == Number(req.params.id)));
  })
  .get("/category/:categoryId", (req, res) => {
    res.send(allProducts.filter((_) => _.category == Number(req.params.categoryId)));
  })
  .post("/", (req, res) => {
    const body = req.body;
    allProducts.push(body);

    res.status(201);
    res.end();
  });

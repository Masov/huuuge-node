import { RequestHandler, Router } from "express";
import { Cart } from "../interfaces/Cart";

const initCart = <Params, Res, Req, Query>(): RequestHandler<Params, Res, Req, Query> => (req, res, next) => {
  req.session!.cart = req.session!.cart || { items: [], total: 0 }
  next()
}

export const shoppingCardRoutes = Router()

  .get<{}, Cart.ResponseDTO>('/', initCart(), (req, res) => {
    res.send({
      items: [], total: 0
    });
  })

  .post<Cart.ItemParams, Cart.Item, Cart.AddItemRequestDTO, {}>(
    '/',
    initCart(),
    (req, res, next) => {
      res.send({})
    })

  .put<
    Cart.ItemParams,
    Cart.Item,
    Cart.AddItemRequestDTO,
    {}
  >('/:lineItemId', initCart(), (req, res, next) => {
    const { productId, amount } = req.body;
    const { cart } = req.session!;
    cart.items
  })

  .delete('/:lineItemId', initCart, (req, res, next) => {

  })
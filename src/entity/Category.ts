import { Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { Product } from "./Product";

@Entity()
export class Category {

  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  name: string = '';

  @OneToMany(type => Product, cat => cat.category)
  products?: Promise<Product[]>

}

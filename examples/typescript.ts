
interface Point { type: 'Point', x: number; }
interface Point { type: 'Point', y: number }

interface Vector { type: 'Vector', x: number; y: number, length: number }
interface Tensor { type: 'Tensor', x: number; y: number, z: number, length: number }

abstract class Point implements Point { }
abstract class Vector implements Vector { multiply() { } }
abstract class Tensor implements Tensor { }

let p: Point = { type: 'Point', x: 123, y: 1234 }
let v: Vector = { type: 'Vector', x: 123, y: 1234, length: 123, multiply() { } }

type Matrix = Point | Vector | Tensor;
let x: Matrix = v as any

let q: string | undefined = {} as any;
if (q) { q.slice }
q && q.slice
q?.slice


if (x.type == 'Vector') {
  x.length
} else {
  // x.length
}

if (x instanceof Point) {
  // x.length
} else if (x instanceof Tensor)
  x.z
else {
  x.multiply()
}

// switch(x.type){
//   case 'Point':
//       x.x;
//   case 'Vector':
//     break;
// }

// dispatch<XEvent>(q:XEvent){}
// dispatch<YEvent>(q:YEvent){}
// dispatch<ZEvent>(q:ZEvent){}

// p = v ;
// v = p ; 

